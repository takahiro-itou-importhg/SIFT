//  -*-  coding: utf-8; mode: c++  -*-  //
/*************************************************************************
**                                                                      **
**          --  SIFT  (Scale Invariant Feature Transform).  --          **
**                                                                      **
**          Copyright (C), 2016, Takahiro Itou                          **
**          All Rights Reserved.                                        **
**                                                                      **
*************************************************************************/

/**
**      An Interface of MySift class.
**
**      @file       Sift/Sift.h
**/

#if !defined( MYSIFTLIB_SIFT_INCLUDED_SIFT_H )
#    define   MYSIFTLIB_SIFT_INCLUDED_SIFT_H

#include    <opencv2/core.hpp>

#include    <list>
#include    <vector>

namespace  Sift  {

//========================================================================
//
//    MySift  class.
//
/**
**    思考エンジンの基底クラス。
**/

class  MySift
{
public:

    typedef     cv::Mat         Graphic;

    typedef     std::vector< std::vector<Graphic> >     GraphicsTable;
    typedef     unsigned  char      TPixel;

    static  const   int     DESCRIPTOR_BIN_SIZE     =  128;

    struct  Descriptor
    {
        double  x;
        double  y;
        double  sig;
        double  arg;

        double  v[DESCRIPTOR_BIN_SIZE];

        Descriptor()
            : x(0.0),
              y(0.0),
              sig(0.0),
              arg(0.0)
        { }

        Descriptor(
                const   double  x_,
                const   double  y_,
                const   double  sig_,
                const   double  arg_)
            : x(x_),
              y(y_),
              sig(sig_),
              arg(arg_)
        { }
    };

    typedef     std::list<Descriptor  *>    DescriptorList;

private:

    static  const   int     KEYPOINT_BIN    =  36;

    struct  KeyPoint
    {
        double  x;                      /**<  座標。        **/
        double  y;                      /**<  座標。        **/
        int     oct;                    /**<  オクターブ。  **/
        int     scale;                  /**<  スケール。    **/

        double  hst[KEYPOINT_BIN];      /**<  ヒストグラム  **/

        KeyPoint()
            : x(0.0),
              y(0.0),
              oct(0),
              scale(0)
        { }

        KeyPoint(
                const  double   x_,
                const  double   y_,
                const  int      o_,
                const  int      s_)
            : x(x_),
              y(y_),
              oct(o_),
              scale(s_)
        { }

        ~KeyPoint() { }
    };

    typedef     std::list<KeyPoint  *>      KeyPointsList;

//========================================================================
//
//    Constructor(s) and Destructor.
//
public:

    //----------------------------------------------------------------
    /**   インスタンスを初期化する
    **  （デフォルトコンストラクタ）。
    **
    **/
    MySift();

    //----------------------------------------------------------------
    /**   インスタンスを破棄する
    **  （デストラクタ）。
    **
    **/
    virtual  ~MySift();

//========================================================================
//
//    Public Member Functions (Implement Pure Virtual).
//

//========================================================================
//
//    Public Member Functions (Overrides).
//

//========================================================================
//
//    Public Member Functions (Pure Virtual Functions).
//

//========================================================================
//
//    Public Member Functions (Virtual Functions).
//

//========================================================================
//
//    Public Member Functions.
//
public:

    //----------------------------------------------------------------
    /**   ガウシアンフィルタを適用する。
    **
    **  @param [in] imgSrc    入力画像。
    **  @param [in] sig       分散。
    **  @param[out] imgDest   出力を受け取る変数。
    **  @return     void.
    **/
    static  void
    applyGaussianFilter(
            const  Graphic  &imgSrc,
            const  double   sig,
            Graphic         &imgDest);

    //----------------------------------------------------------------
    /**   特徴量を計算する。
    **
    **  @param [in] tblL
    **  @param [in] kpList
    **  @param [in] tblPos
    **  @param [in] tblArg
    **  @param[out] desList
    **  @return     void.
    **/
    void
    calcDescriptor(
            const   GraphicsTable   &tblL,
            const   KeyPointsList   &kpList,
            const   GraphicsTable   &tblPow,
            const   GraphicsTable   &tblArg,
            DescriptorList          &dscList);

    //----------------------------------------------------------------
    /**   オリエンテーションを計算する。
    **
    **  @param [in] tblL
    **  @param [in] kpList
    **  @param[out] tblPos
    **  @param[out] tblArg
    **  @return     void.
    **/
    void
    calcOrientation(
            const   GraphicsTable   &tblL,
            KeyPointsList           &kpList,
            GraphicsTable           &tblPow,
            GraphicsTable           &tblArg);

    //----------------------------------------------------------------
    /**   特徴量記述子の内容を描画する。
    **
    **  @param [in]     itrHead   先頭の特徴量記述子。
    **  @param [in]     itrTail   末尾の特徴量記述子。
    **  @param [in,out] imgTrg    描画対象の画像。
    **  @return     void.
    **/
    template  <typename  TKIter>
    static  void
    drawDescriptors(
            const   TKIter  itrHead,
            const   TKIter  itrTail,
            Graphic         &imgTrg);

    //----------------------------------------------------------------
    /**   キーポイントを描画する。
    **
    **  @param [in]     itrHead   先頭のキーポイント。
    **  @param [in]     itrTail   末尾のキーポイント。
    **  @param [in,out] imgTrg    描画対象の画像。
    **  @return     void.
    **/
    template  <typename  TKIter>
    static  void
    drawKeyPoints(
            const   TKIter  itrHead,
            const   TKIter  itrTail,
            Graphic         &imgTrg);

    //----------------------------------------------------------------
    /**   アルゴリズムを実行する。
    **
    **  @note   結果はカラー画像で返す。
    **  @param [in] imgSrc    入力画像。
    **  @param[out] imgOut    出力画像。
    **  @return     void.
    **/
    void
    executeSIFT(
            const  Graphic  &imgSrc,
            Graphic         &imgOut);

    //----------------------------------------------------------------
    /**   アルゴリズムを実行する。
    **
    **  @note   結果はグレースケール画像で返す。
    **  @param [in] imgSrc    入力画像。
    **  @param[out] imgGray   入力のグレースケール画像。
    **  @param[out] imgOut    出力画像。
    **  @return     void.
    **/
    void
    executeSIFT(
            const  Graphic  &imgSrc,
            Graphic         &imgGray,
            Graphic         &imgOut);

    //----------------------------------------------------------------
    /**   アルゴリズムを実行する。
    **
    **/
    void
    executeSIFT(
            const  Graphic  &imgSrc,
            DescriptorList  &dscList);

    //----------------------------------------------------------------
    /**   スケールとキーポイント検出。
    **
    **  @param [in] imgSrc    入力画像。
    **  @param[out] tblL      平滑化画像用バッファ。
    **  @return     void.
    **/
    void
    getKeyPoints(
            const  Graphic  &imgSrc,
            GraphicsTable   &tblL,
            GraphicsTable   &tblDoG,
            KeyPointsList   &keys);

    //----------------------------------------------------------------
    /**   キーポイントのローカライズ。
    **
    **  @param [in] imgSrc    入力画像。
    **  @return     void.
    **/
    static  void
    localizeKeyPoints(
            const  Graphic          &imgSrc,
            const  GraphicsTable    &tblDoG,
            KeyPointsList           &kpList);

//========================================================================
//
//    Protected Member Functions.
//

//========================================================================
//
//    For Internal Use Only.
//

//========================================================================
//
//    For Internal Use Only.
//
private:

    //----------------------------------------------------------------
    /**   画像の差分。
    **
    **/
    static  inline  void
    diffGraphics(
            const  Graphic  &imgLhs,
            const  Graphic  &imgRhs,
            Graphic         &imgDest);

    //----------------------------------------------------------------
    /**   ダウンサンプリング。
    **
    **  @param [in] imgSrc    入力画像。
    **  @param [in] sig       分散。
    **  @param[out] imgDest   出力を受け取る変数。
    **  @return     void.
    **/
    static  inline  void
    downSampling(
            const  Graphic  &imgSrc,
            Graphic         &imgDest);

    //----------------------------------------------------------------
    /**   逆行列の計算。
    **
    **/
    template  <int  N>
    static  inline  void
    calcInvMatrix(
            double  dMat[N][N],
            double  dInv[N][N]);

    //----------------------------------------------------------------
    /**   画像からピクセル値の参照を取り出す。
    **
    **  @param [in] imgSrc
    **  @param [in] sx
    **  @param [in] sy
    **/
    static  inline  const   TPixel  &
    getPixel(
            const  Graphic  &imgSrc,
            const  int      sx,
            const  int      sy)
    {
        return ( getPixelT<TPixel>(imgSrc,  sx,  sy) );
    }

    //----------------------------------------------------------------
    /**   画像からピクセル値の参照を取り出す。
    **
    **  @note   ただし取り出す値の型は整数に限定しない。
    **  @tparam     T
    **  @param [in] tblSrc
    **  @param [in] sx
    **  @param [in] sy
    **  @return
    **/
    template  <typename  T>
    static  inline  const   T  &
    getPixelT(
            const  Graphic  &imgSrc,
            const  int      sx,
            const  int      sy);

    //----------------------------------------------------------------
    /**   特徴量を生成する。
    **
    **  @param [in]     tblPow
    **  @param [in]     tblArg
    **  @param [in,out] ptrDsc
    **  @return     void.
    **/
    static  inline  double
    makeDescriptor(
            const  Graphic  &tblPow,
            const  Graphic  &tblArg,
            Descriptor  *   ptrDsc);

    //----------------------------------------------------------------
    /**   ガウシアンフィルタを作成する。
    **
    **  @param [in] sig
    **  @param[out] imgMsk
    **  @return     void.
    **/
    static  inline  void
    makeGaussianFilter(
            const   double  sig,
            Graphic         &imgMsk);

    //----------------------------------------------------------------
    /**   画像からピクセル値の参照を取り出す。
    **
    **  @param [in] imgTrg
    **  @param [in] dx
    **  @param [in] dy
    **/
    static  inline  TPixel  &
    setPixel(
            Graphic     &imgTrg,
            const  int  dx,
            const  int  dy)
    {
        return ( setPixelT<TPixel>(imgTrg,  dx,  dy) );
    }

    //----------------------------------------------------------------
    /**   画像からピクセル値の参照を取り出す。
    **
    **  @note   ただし取り出す値の型は整数に限定しない。
    **  @tparam     T
    **  @param [in] imgTrg
    **  @param [in] dx
    **  @param [in] dy
    **/
    template  <typename  T>
    static  inline  T  &
    setPixelT(
            Graphic     &imgTrg,
            const  int  dx,
            const  int  dy);

//========================================================================
//
//    Member Variables.
//
private:

    /**   オクターブ数。    **/
    size_t      m_numOcts;

//========================================================================
//
//    Other Features.
//
private:
    typedef     MySift      This;
    MySift              (const  This  &);
    This &  operator =  (const  This  &);
public:
    //  テストクラス。  //
    friend  class   MySiftTest;
};

}   //  End of namespace  Sift

#endif
