
##----------------------------------------------------------------
##
##    Package Configurations.
##    パッケージ設定。
##

##
##    要求するツールのバージョンを指定する。
##

cmake_minimum_required (VERSION  3.0.2)

##
##    パッケージの情報を指定する。
##

project (SIFT)
set (serial  "0.0.0")

##----------------------------------------------------------------
##
##    プロジェクトの基本設定。
##

option (ENABLE_WIN32  "Enable Win32 API."  ON)

set (CMAKE_C_FLAGS    "-std=c++11")
set (CMAKE_CXX_FLAGS  "-std=c++11")
set (SIFT_INCLUDE_DIR  "Include/Sift")
set (SOURCE_INCLUDE_DIR  "${PROJECT_SOURCE_DIR}/${SIFT_INCLUDE_DIR}")
set (BINARY_INCLUDE_DIR  "${PROJECT_BINARY_DIR}/${SIFT_INCLUDE_DIR}")
set (SOURCE_CMAKE_DIR    "${PROJECT_SOURCE_DIR}/Modules")

##
##    Check Header Files.
##

##
##    Check Types.
##

##
##    Check Compiler Features.
##

##----------------------------------------------------------------
##
##    外部パッケージの検索。
##

##
##    ここに OpenCV のインストール場所を指定する。
##

set (OpenCV_DIR  "N:/OpenCV/2.4.13/Vc12")   ##  OpenCVConfig.cmake がある場所
include_directories ("${OpenCV_DIR}/include")

find_package (OpenCV  REQUIRED)


##----------------------------------------------------------------
##
##    設定を書き込んだファイルを生成。
##

##
##    インクルードパスの設定。
##

include_directories ("${PROJECT_SOURCE_DIR}/Include")
include_directories ("${PROJECT_BINARY_DIR}/Include")

##----------------------------------------------------------------
##
##    サブディレクトリの追加。
##

add_subdirectory (Lib)
add_subdirectory (Bin)

